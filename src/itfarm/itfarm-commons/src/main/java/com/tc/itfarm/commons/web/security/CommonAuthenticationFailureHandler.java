package com.tc.itfarm.commons.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;
/**
 * 
 * @Description: 认证失败自定义处理类   
 * @author: wangdongdong  
 * @date:   2016年7月11日 下午4:15:42   
 *
 */
@Service("commonAuthenticationFailureHandler")
public class CommonAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		/**
		 * 记录登陆失败日志
		 */
		request.setAttribute("errorMsg", exception.getMessage());
		request.getRequestDispatcher("/login.do").forward(request, response);
	}
}
