<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
    <jsp:include page="../header.jsp"></jsp:include>
    <div id="dcMain">
        <!-- 当前位置 -->
        <div id="urHere">ITFARM 管理中心<b>></b><strong>自定义菜单栏</strong> </div>   <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
        <h3><a href="${ctx}/menu/addUI.do" class="actionBtn">添加菜单</a>自定义菜单栏</h3>
        <div class="navList">
            <ul class="tab">
                <li><a class="selected">主导航</a></li>
            </ul>
            <table width="100%" border="0" cellpadding="10" cellspacing="0" class="tableBasic">
                <tr>
                    <th width="113" align="center">导航名称</th>
                    <th align="left">链接地址</th>
                    <th width="80" align="center">排序</th>
                    <th width="120" align="center">操作</th>
                </tr>
                <c:forEach items="${menus}" var="menu">
                <tr>
                    <td>${menu.name}</td>
                    <td>${menu.url}</td>
                    <td align="center">${menu.orderNo}</td>
                    <td align="center"><a href="${ctx}/menu/editUI.do?id=${menu.recordId}">编辑</a> | <a href="${ctx}/menu/delete.do?id=${menu.recordId}">删除</a></td>
                </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    </div>
    <div class="clear"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
    <div class="clear"></div> </div>
</body>
</html>