package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.exception.BusinessException;
import com.tc.itfarm.api.model.PageCriteria;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.service.BaseService;

import java.util.List;

/**
 * Created by Administrator on 2016/8/14.
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    protected abstract SingleTableDao getSingleDao();

    @Override
    public Integer insert(T entity) {
        return getSingleDao().insert(entity);
    }

    @Override
    public Integer update(T entity) {
        return getSingleDao().updateById(entity);
    }

    @Override
    public Integer updateBySelective(T entity) {
        return getSingleDao().updateByIdSelective(entity);
    }

    @Override
    public Integer delete(Integer id) {
        return getSingleDao().deleteById(id);
    }

    @Override
    public T select(Integer id) {
        return (T) getSingleDao().selectById(id);
    }

    @Override
    public List<T> selectAll() {
        return getSingleDao().selectByCriteria(null);
    }
}
