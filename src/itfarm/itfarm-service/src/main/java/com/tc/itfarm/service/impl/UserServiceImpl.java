package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.EncoderUtil;
import com.tc.itfarm.api.util.PageQueryHelper;
import com.tc.itfarm.dao.UserDao;
import com.tc.itfarm.model.User;
import com.tc.itfarm.model.UserCriteria;
import com.tc.itfarm.model.UserRole;
import com.tc.itfarm.service.UserRoleService;
import com.tc.itfarm.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	@Resource
	private UserDao userDao;
	@Resource
	private UserRoleService userRoleService;
	
	@Override
	public Integer save(User user) {
		Assert.notNull(user, "用户对象不能为空!");
		Assert.isTrue(StringUtils.isNotBlank(user.getUsername()), "用户名不能为空!");
		user.setModifyTime(DateUtils.now());
		user.setPassword(EncoderUtil.md5(user.getPassword()));
		if (user.getRecordId() == null) {
			user.setRegisterTime(DateUtils.now());
			return userDao.insert(user);
		}
		return userDao.updateByIdSelective(user);
	}

	@Override
	public PageList<User> selectUserByPage(String username, String nickname, Date startDate, Date endDate, Page page) {
		UserCriteria criteria = new UserCriteria();
		UserCriteria.Criteria innerCriteria = criteria.createCriteria();
		if (StringUtils.isNotBlank(username)) {
			innerCriteria.andUsernameLike("%" + username + "%");
		}
		if (StringUtils.isNotBlank(nickname)) {
			innerCriteria.andNicknameLike("%" + nickname + "%");
		}
		if (startDate != null) {
			innerCriteria.andRegisterTimeGreaterThanOrEqualTo(startDate);
		}
		if (endDate != null) {
			innerCriteria.andRegisterTimeLessThan(endDate);
		}
		return PageQueryHelper.queryPage(page, criteria, userDao, null);
	}

	@Override
	public User selectByUsername(String username) {
		Assert.isTrue(StringUtils.isNotBlank(username), "用户名不能为空!");
		UserCriteria criteria = new UserCriteria();
		criteria.or().andUsernameEqualTo(username);
		List<User> userList = userDao.selectByCriteria(criteria);
		return userList.size() > 0 ? userList.get(0) : null;
	}

	@Override
	public User select(String username, String password) {
		Assert.isTrue(StringUtils.isNotBlank(username), "用户名不能为空!");
		Assert.isTrue(StringUtils.isNotBlank(password), "密码不能为空!");
		UserCriteria criteria = new UserCriteria();
		criteria.or().andUsernameEqualTo(username).andPasswordEqualTo(password);
		List<User> userList = userDao.selectByCriteria(criteria);
		return userList.size() > 0 ? userList.get(0) : null;
	}

	@Override
	public Integer save(User user, Integer[] roleIds) {
		if (user.getRecordId() != null) {
			userRoleService.deleteByUserId(user.getRecordId());
		}
		this.save(user);
		// 直接插入所有
		for (Integer rId : roleIds) {
			UserRole ur = new UserRole();
			ur.setUserId(user.getRecordId());
			ur.setRoleId(rId);
			ur.setCreateTime(DateUtils.now());
			userRoleService.insert(ur);
		}
		return roleIds.length;
	}

	@Override
	public Integer delete(Integer id) {
		super.delete(id);
		return userRoleService.deleteByUserId(id);
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return userDao;
	}
}
