package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.exception.BusinessException;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.dao.SystemConfigDao;
import com.tc.itfarm.model.SystemConfig;
import com.tc.itfarm.service.ArticleService;
import com.tc.itfarm.service.MenuService;
import com.tc.itfarm.service.SystemConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wangdongdong on 2016/8/29.
 */
@Service
public class SystemConfigServiceImpl extends BaseServiceImpl<SystemConfig> implements SystemConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemConfigServiceImpl.class);
    @Resource
    private SystemConfigDao systemConfigDao;
    @Resource
    private ArticleService articleService;
    @Resource
    private MenuService menuService;

    @Override
    protected SingleTableDao getSingleDao() {
        return systemConfigDao;
    }

    @Override
    public Integer insert(SystemConfig entity) {
        try {
            throw new BusinessException("不允许添加系统配置数据");
        } catch (BusinessException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public List<SystemConfig> selectAll() {
        List<SystemConfig> systemConfigs = super.selectAll();
        if (systemConfigs.size() < 1) {
            SystemConfig systemConfig = new SystemConfig();
            systemConfig.setWebName("itfarm-web");
            systemConfig.setAdminName("itfarm-admin");
            systemConfig.setWebTitle("程序猿的世界，你不懂");
            systemConfig.setAdminTitle("ITFARM后台管理中心");
            systemConfig.setAdminMenuCount(articleService.selectAll().size());
            systemConfig.setWebMenuCount(menuService.selectAll().size());
            systemConfig.setAdminMenuCount(menuService.selectAll().size());
            systemConfig.setEncoding("UTF-8");
            systemConfig.setLanguage("zh_cn");
            systemConfig.setCreateTime(DateUtils.now());
            systemConfig.setModifyTime(DateUtils.now());
            systemConfigDao.insert(systemConfig);
            systemConfigs.add(systemConfig);
        }
        return systemConfigs;
    }
}
