package com.tc.itfarm.web.vo;

import com.tc.itfarm.model.User;

import java.util.List;

/**
 * Created by Administrator on 2016/9/5.
 */
public class UserVO extends User {
    private List<Integer> favoriteIds;

    public List<Integer> getFavoriteIds() {
        return favoriteIds;
    }

    public void setFavoriteIds(List<Integer> favoriteIds) {
        this.favoriteIds = favoriteIds;
    }
}
